# HTML && CSS && JS - Final Project

## Description

Your final project will be a platform for you to combine and practice all of the material covered in HTML&CSS&JS classes.

### Why this assignment?

This assignment will give you a chance to revisit and solidify your understanding of what has been covered. It will also allow you to **create a portfolio worthy project.** Or future worthy project.

## Project requirements

Your final project should take all of the ideas from class and put them together.

**Your final project should incorporate everything listed below. **

- HTML - Use semantic HTML
- CSS
  - Typographic styles
  - Flex Box
  - CSS Grid
- JavaScript
  - Variables and Functions
  - JS and DOM
  - Event Listeners

**You also must publish your project as a [GitHub Pages site](https://docs.github.com/en/github/working-with-github-pages/creating-a-github-pages-site) on GitHub.** Remember this is a portfolio worthy project, and we want to make sure you are able to show this off!

This may sound like a lot at first but you can borrow ideas and code snippets from the class examples.

## Content and Subject for Final Project

I would like to give **2 options** for this project:

1. You want to research the subject. You can **coordinate** the idea with your Instructor or TA's.

2. Or you can take your Portfolio Project you made in HTML&CSS final project and incorporate some of the javascript features


## Ideas

Here's some ideas


1. Continue making interactive **single page site.** 

If you previously did this option, now its time to add some javascript.

Make a single page site. This should be one web page with content that is **revealed as youscroll** down the page.

- https://onepagelove.com
- https://www.awwwards.com/websites/single-page/


2. Continue imporving your Personal Website. 

If you previously did this option, now its time to add some javascript.

- https://w3layouts.com/template-category/personal/

**Other ideas that are not related to above 2:**

- Maybe you are someone who is into gaming? You can try to make a game using javascript. Check out these 2 libraries:
    1. https://www.pixijs.com/
    2. https://phaser.io/

It is okay even if you project is clone of some examples.

## Layout

Think of a single page site as a stack of boxes. Think of each box in this stack as it's own "page." Each of these sections can have their own layout. One section could be arranged as a grid, while another could use flex box.

**You should be using Flexbox for more of your layout.**

Take a look at this wire frame for a single page site.

![Wireframe](Wireframe.png)

Here is the same image with some notes. The red and blue lines show the main axis and the cross axis.

![Wire frame with flex notes ](Wireframe-with-flex-notes.png)

## Deliverable

Any single page site or portfolio site incorporated some javascript functionalities. Or game, music etc platforms.

## Due date

You will submit the repo to us 1 day JavaScript Interview!

## Optional

1. You will publish your project as a **[GitHub Pages site](https://docs.github.com/en/github/working-with-github-pages/creating-a-github-pages-site) on GitHub.** Remember this is a portfolio worthy project, and we want to make sure you are able to show this off!

2. Make site interactive using JavaScript (This will be done after you learn JS)
